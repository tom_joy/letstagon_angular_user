import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
  providers:[AdminService]
})
export class ReviewComponent implements OnInit {
  userid;
  eventid;
  experiencename;
  eventschedule;
  location;
  data;
  message;
  subject;
  rating;
  fileurl;
  form: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.eventid = localStorage.getItem('event_id');
   
    this.adminService.getEvent(this.eventid,this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        this.data = res.data;
        $('.loading-spinner').fadeOut('slow');
        this.experiencename = this.data.experience_name;
        this.eventschedule = this.data.date_scheduled;
        this.location = this.data.address;
        this.message = this.data.message;
        this.subject = this.data.subject;
        this.rating = this.data.rating;
        this.fileurl = this.data.image_path;
      }
      else {
        $('.loading-spinner').fadeOut('slow');
            alert(res.message);
      }
     
      

    });
  }

}
