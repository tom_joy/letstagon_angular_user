import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlyfooterComponent } from './onlyfooter.component';

describe('OnlyfooterComponent', () => {
  let component: OnlyfooterComponent;
  let fixture: ComponentFixture<OnlyfooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlyfooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlyfooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
