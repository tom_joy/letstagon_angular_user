import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from "@angular/router";
@Component({
  selector: 'app-onlyfooter',
  templateUrl: './onlyfooter.component.html',
  styleUrls: ['./onlyfooter.component.css']
})
export class OnlyfooterComponent implements OnInit {
  homeForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,private router: Router) { }

  ngOnInit() {
    this.homeForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
  });
  }
// convenience getter for easy access to form fields
get f() { return this.homeForm.controls; }
 
onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.homeForm.invalid) {
        return;
    }

         this.router.navigate(['/']);

}
}
