import { Component, OnInit } from '@angular/core';
import { AdminService } from "../service/admin.service";
import { AuthenticationService } from "../service/auth.service";

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css'],
  providers: [AdminService, AuthenticationService]
})
export class PrivacyPolicyComponent implements OnInit {

  constructor(private adminService: AdminService) { }

  ngOnInit() {
  }

}
