import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Experience } from "../model/experience.model";
import { Ngo } from "../model/ngo.model";


@Injectable()
export class AdminService {
  experience;
  events
  apiurl = environment.apiUrl;
  constructor(private http: HttpClient) {
  }

  //Getiing list of experiences
  experiencelist() {
    return this.http.get(this.apiurl + "/admin/experience_list")
      .map(res => res);
  }
  //Home experience list
  homeexperiencelist() {
    return this.http.get(this.apiurl + "/admin/experience_list_limit")
      .map(res => res);
  }
  //Creating an Experience
  createExperience(experience: Experience) {
    console.log(experience);
    return this.http.post(this.apiurl + "/admin/create_master_experience", experience);
  }

  //Deleting an Experience
  deleteExperience(id: number) {
    return this.http.post(this.apiurl + "/admin/delete_experience", { experience_id: id })
      .map(res => res);
  }
  //For Editing Particular Experience
  getExperienceById(id: number) {
    return this.http.post(this.apiurl + "/admin/experience_id", { experience_id: id })
      .map(res => res);
  }
  //For updating srevices
  updateExperience(experience: Experience) {
    return this.http.post(this.apiurl + "/admin/edit_master_experience", experience).map(res => res);
  }


  //For updating srevices
  opendetailedexperience(experience: Experience) {
    return this.http.post(this.apiurl + "/admin/experience_detailed_view", { experience_id: experience }).map(res => res);
  }


  //Tagging to particular experience
  tagon(eventid, userid) {
    return this.http.post(this.apiurl + "/admin/usertagged", { event_id: eventid, user_id: userid }).map(res => res);
  }
  //For applied oppurtunities
  individualOppurtunity(userid) {

    return this.http.post(this.apiurl + "/user/individual_opportunity_list", { "user_id": userid }).map(res => res);
  }
  //For attended oppurtunities
  individualattendedOppurtunity(userid) {

    return this.http.post(this.apiurl + "/user/individual_attended_opportunity_list", { "user_id": userid }).map(res => res);
  }


  //Approve the attendance
  approveAttendance(eventid, userid) {
    return this.http.post(this.apiurl + "/admin/self_attendance", { "user_tag_id": eventid, "attendance_flag": "2", 'user_id': userid });
  }

  //For request oppurtunities
  individualrequestOppurtunity(userid) {

    return this.http.post(this.apiurl + "/user/requested_user_opportunity_list", { "user_id": userid }).map(res => res);
  }


  //Getting Individual Profile
  getProfile(userid) {
    return this.http.post(this.apiurl + "/user/user_profile1", { 'user_id': userid });
  }
  //Updating user profile
  updateProfile(user) {
    return this.http.post(this.apiurl + "/user/edit_volunteer_profile", user);
  }

  //View Testimonial
  viewTestimonial(userid) {
    return this.http.post(this.apiurl + "/user/view_individual_testimonial", { 'user_id': userid });
  }

  //testimonial list
  viewtestimonial() {
    return this.http.get(this.apiurl + "/user/view_testimonials")

  }
  //Getting Testimonial
  gettestimonial(userid) {
    return this.http.post(this.apiurl + "/user/view_editing_testimonial", { 'user_id': userid });
  }
  //Updating Testimonial
  updateTestimonial(testimonial) {
    return this.http.post(this.apiurl + "/user/edit_testimonial", testimonial);
  }
  //Add Testimonial
  addTestimonial(subject, message, userid) {

    let testimonial = {
      "subject": subject,
      "message": message,
      "user_id": userid
    }
    return this.http.post(this.apiurl + "/user/add_testimonials", testimonial);
  }
  //Get event
  getEvent(eventid, userid) {
    return this.http.post(this.apiurl + "/user/reviewuser", { "event_id": eventid, "user_id": userid })
      .map(res => res);
  }
  //Add Review
  addReview(subject, message, rating, userid, eventid, experienceid) {

    let data =
    {
      "user_id": userid,
      "message": message,
      "subject": subject,
      "experience_id": experienceid,
      "eventid": eventid,
      "rating": rating
    }
    return this.http.post(this.apiurl + "/user/reviews", data)
      .map(res => res);
  }
  //Update review
  updateReview(subject, message, rating, userid, eventid, experienceid) {

    let data =
    {
      "user_id": userid,
      "message": message,
      "subject": subject,
      "experience_id": experienceid,
      "event_id": eventid,
      "rating": rating
    }
    return this.http.post(this.apiurl + "/user/update_review", data)
      .map(res => res);
  }
  //Get Review
  getReview(userid, eventid, experienceid) {
    let data =
    {
      "user_id": userid,
      "experience_id": experienceid,
      "event_id": eventid

    }
    return this.http.post(this.apiurl + "/user/edit_review_information", data)
      .map(res => res);
  }
  //Get Experience details
  getExperiencedetails(eventid) {
    return this.http.post(this.apiurl + "/user/getexperiencedetails", { "event_id": eventid })
      .map(res => res);
  }
  // Get cities
  getCities() {
    return this.http.get(this.apiurl + "/admin/citylist")
      .map(res => res);
  }
  //Get filtered List
  getFilteredlist(filteredlist) {
    return this.http.post(this.apiurl + "/admin/experience_list", filteredlist)
      .map(res => res);
  }

  getFilteredlistcatogery(city, category, events) {


    if (city == undefined) {
      city = "";
    }
    if (category == undefined) {
      category = "";
    }
    var search_category = {
      "city": city,
      "category": category,
      "events": events
    };
    return this.http.post(this.apiurl + "/admin/experience_list", search_category)
      .map(res => res);
  }
  //Dashboard Count
  dashboardCount(userid) {
    return this.http.post(this.apiurl + "/user/dashboard_count", { "user_id": userid })
      .map(res => res);
  }

  //Send email

  Sendcontactmail(contact) {
    return this.http.post(this.apiurl + "/cms/insert_contact_message", contact)
      .map(res => res);
  }

  //Custom Schedule
  Customrequest(userid, eventid, organization, nov, description, date, location) {
    var body =
    {
      "user_id": userid,
      "event_id": eventid,
      "organisation": organization,
      "request_date": date,
      "no_of_volunteers": nov,
      "description": description,
      "location": location
    }
    return this.http.post(this.apiurl + "/user/insert_requested_opportunity", body)
      .map(res => res);
  }


  //Review list

  Reviewlists(experienceid) {
    return this.http.post(this.apiurl + "/user/reviewslistbyevent", { "exp_id": experienceid })
      .map(res => res);
  }

  //Counters
  Counters() {
    return this.http.get(this.apiurl + "/admin/counters")
      .map(res => res);
  }

  locations(userid, experienceid) {
    return this.http.post(this.apiurl + "/admin/event_locations", { "user_id": userid, "experience_id": experienceid })
      .map(res => res);
  }

  ngoinsert(ngo: Ngo) {
    // console.log(ngo);
    return this.http.post(this.apiurl + "/admin/create_ngo_admin", ngo);
  }
  coedsend(contact) {
    return this.http.post(this.apiurl + "/user/institution_request", contact)
      .map(res => res);
  }
  getcategorylist() {
    return this.http.get(this.apiurl + "/user/view_category")
      .map(res => res);
  }
  getstatelist() {
    return this.http.get(this.apiurl + "/admin/statelist_active")
      .map(res => res);
  }
  getcitybystate(id) {

    return this.http.post(this.apiurl + "/admin/getcity_wrtstate", { state_id: id })
      .map(res => res);
  }

}
