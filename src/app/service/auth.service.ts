import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { User } from "../model/user.model";

@Injectable()
export class AuthenticationService {
	apiurl = environment.apiUrl;
	constructor(private http: HttpClient) {
	}

	//For login function
	checklogin(email, pwd) {
		return this.http.post(this.apiurl + "/user/login", { email: email, password: pwd })
			.map(res => res);
	}
	//For User Registration
	userRegister(user: User) {
		return this.http.post(this.apiurl + "/user/register", user)
			.map(res => res);
	}
	//For User Activation
	userActivation(userid) {
		return this.http.post(this.apiurl + "/user/user_activation", { user_id: userid })
			.map(res => res);
	}
	//Forgot password
	forgotPassword(email) {
		return this.http.post(this.apiurl + "/user/forgetpassword", { email: email })
			.map(res => res);
	}
	//Reset password
	resetPassword(userid, password) {
		return this.http.post(this.apiurl + "/user/resetpassword", { id: userid, password: password })
			.map(res => res);
	}
	//Check Reset Status 
	checkResetstatus(userid) {
		return this.http.post(this.apiurl + "/user/checkresetstatus", { id: userid })
			.map(res => res);
	}
	//Check Reset Status 
	changePassword(userid,old,current,confirm) {
		let body = {"user_id":userid,"current_password":old,"new_password": current,"confirm_password":confirm};
		return this.http.post(this.apiurl + "/user/change_user_password",body).map(res => res);
	}

	//Social Media Login
	sociallogin(id,name,email) {
		let body = {"app_id":id,"email":email,"first_name": name,"password":"Social Login"};
		return this.http.post(this.apiurl + "/user/social_media_register",body).map(res => res);
	}
}
