import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.component.html',
  styleUrls: ['./edit-review.component.css'],
  providers: [AdminService]
})
export class EditReviewComponent implements OnInit {

  userid;
  eventid;
  addReview: FormGroup;
  submitted = false;
  experienceid;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }


  ngOnInit() {
    $('.loading-spinner').fadeOut('slow');
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.eventid = localStorage.getItem('event_id');
    this.experienceid = localStorage.getItem('experience_id');

    this.adminService.getReview(this.userid, this.eventid, this.experienceid).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        this.addReview.setValue(res.data);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
    this.addReview = this.formBuilder.group({
      message: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      rating: ['', [Validators.required]],
    });
  }
  get f() { return this.addReview.controls; }

  onSubmit() {
    $('.loading-spinner').show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.addReview.invalid) {
      $('.loading-spinner').fadeOut('slow');
      this.submitted = true;
      return;
    }
    this.adminService.updateReview(this.addReview.value.subject, this.addReview.value.message, this.addReview.value.rating, this.userid, this.eventid, this.experienceid).subscribe((res: any) => {
      if ((res.status == "success") && (localStorage.getItem('homepage') !== 'true'))  {
        $('.loading-spinner').fadeOut('slow');
        this.router.navigate(['/', 'reviews']);
      
      }else if ((res.status == "success") && (localStorage.getItem('homepage') == 'true'))  {
        this.router.navigate(['/','home']);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
  }
}
