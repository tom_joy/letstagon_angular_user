import { Component, OnInit } from '@angular/core';
declare var $: any;
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from "../service/auth.service";
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [AuthenticationService]
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private flashMessage: FlashMessagesService,private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
	$('.loading-spinner').hide();
    this.signupForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phone: ['', [Validators.required,Validators.pattern(/^[6-9]\d{9}$/)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  // , Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  onSubmit() {
	  $('.loading-spinner').show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.signupForm.invalid) {
		$('.loading-spinner').hide();
      return;
    }

    this.authService.userRegister(this.signupForm.value).subscribe((res: any) => {
      if (res.status == "success") {
		$('.loading-spinner').hide();
    this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
    console.log(res.message);
        // this.router.navigate(['/', 'signin']);
      }
      else {
		$('.loading-spinner').hide();
    this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });

  }
}
