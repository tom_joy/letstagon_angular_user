import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers:[AdminService]
})
export class ProfileComponent implements OnInit {
  userid;
  firstname;
  lastname;
  dob;
  email;
  phone;
  designation;
  organization;
  fileurl;
  interests;
  skills;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    $('.loading-spinner').show();
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }

    this.adminService.getProfile(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        this.firstname = res.data.first_name;
        this.lastname = res.data.last_name;
        this.dob = res.data.dob;
        this.email = res.data.email;
        this.organization = res.data.organization;
        this.designation = res.data.designation;
        this.phone = res.data.phone;
        this.skills = res.data.skills;
        this.interests = res.data.interests;
        this.fileurl = res.data.file;
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        alert(res.message);
      }

    });

  }

}
