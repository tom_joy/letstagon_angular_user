import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { AuthenticationService } from "../service/auth.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $:any;
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers:[AuthenticationService]
})
export class ResetPasswordComponent implements OnInit {
  userid;
  resetForm: FormGroup;
  submitted = false;
  constructor(private route: ActivatedRoute,private router:Router,private authService:AuthenticationService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
  });
    this.route.params.subscribe(params => {
      this.userid = params['id'];
      this.authService.checkResetstatus(this.userid).subscribe((res: any) => {
				if (res.status == "success") {
                localStorage.removeItem('notactivated');
                $('.loading-spinner').hide();
				}
				else {
          localStorage.setItem('notactivated',res.message);
          this.router.navigate(['/','signin']);	
				}

			});
    });
  }
  get f() { return this.resetForm.controls; }

  onSubmit() {
    this.submitted = true;
    $('.loading-spinner').show();
    if (this.resetForm.invalid) {
      $('.loading-spinner').fadeOut('slow');
        return;
    }
    this.authService.resetPassword(this.userid,this.resetForm.value.password).subscribe((res: any)=>{
      if(res.status == "success"){
        $('.loading-spinner').fadeOut('slow');
        localStorage.removeItem('activateid');
        localStorage.setItem('activateid',"Password reset successfully");
        this.router.navigate(['/','signin']);
      }
    });
}
}
