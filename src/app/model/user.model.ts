export class User {
    id: number;
    first_name: string;
    email: string;
    password:string;
    confirmPassword:string;
    phone:number;
  }
  