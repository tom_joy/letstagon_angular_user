export class Experience {
  id: number;
  experience_name: string;
  cause:string;
  description: string;
  file:any;
}
