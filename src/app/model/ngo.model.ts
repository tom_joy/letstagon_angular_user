export class Ngo {
    id: number;
    ngo_name: string;
    ngo_type:string;
    ngo_description:string;
    contact_person:string;
    contact_number: number;
    email:string;
    address:string;
    country:string;
    state:string;
    city:string;
  }