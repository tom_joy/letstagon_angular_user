import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-edit-testimonial',
  templateUrl: './edit-testimonial.component.html',
  styleUrls: ['./edit-testimonial.component.css'],
  providers: [AdminService]
})
export class EditTestimonialComponent implements OnInit {
  edittestimonial: FormGroup;
  userid;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    $('.loading-spinner').hide();
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }

    this.adminService.gettestimonial(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        this.edittestimonial.setValue(res.data);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }

    });
    this.edittestimonial = this.formBuilder.group({
      message: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      id: ['', [Validators.required]],
      user_id:['', [Validators.required]],
    });

  }
  get f() { return this.edittestimonial.controls; }

  onSubmit() {
    $('.loading-spinner').show();
    // stop here if form is invalid
    if (this.edittestimonial.invalid) {
      $('.loading-spinner').fadeOut('slow');
      this.submitted =true;
      return;
    }
    this.adminService.updateTestimonial(this.edittestimonial.value).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        this.router.navigate(['/', 'testimonial']);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
  }
}
