import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTestinomialComponent } from './add-testinomial.component';

describe('AddTestinomialComponent', () => {
  let component: AddTestinomialComponent;
  let fixture: ComponentFixture<AddTestinomialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTestinomialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTestinomialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
