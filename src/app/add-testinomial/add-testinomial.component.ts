import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';

@Component({
  selector: 'app-add-testinomial',
  templateUrl: './add-testinomial.component.html',
  styleUrls: ['./add-testinomial.component.css'],
  providers:[AdminService]
})
export class AddTestinomialComponent implements OnInit {
  submitted = false;
  testimonialForm: FormGroup;
  userid;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    $('.loading-spinner').show();
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.adminService.gettestimonial(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        this.router.navigate(['/','testimonial']);

      }
      else {
        $('.loading-spinner').hide();
      }

    });
    this.testimonialForm = this.formBuilder.group({
      message: ['', [Validators.required]],
      subject: ['', [Validators.required]],
    });

  }
  get f() { return this.testimonialForm.controls; }

  onSubmit() {
    $('.loading-spinner').show();
    this.submitted=true;
    // stop here if form is invalid
    if (this.testimonialForm.invalid) {
      $('.loading-spinner').hide();
      $('.loading-spinner').fadeOut('slow');
      return;
    }
    this.adminService.addTestimonial(this.testimonialForm.value.subject,this.testimonialForm.value.message,this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        this.router.navigate(['/', 'testimonial']);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
  }

}
