import { Component, OnInit } from '@angular/core';
import { AdminService } from "../service/admin.service";
import { Router } from "@angular/router";
import { FlashMessagesService } from 'angular2-flash-messages';
declare var $: any;

@Component({
  selector: 'app-request-opp',
  templateUrl: './request-opp.component.html',
  styleUrls: ['./request-opp.component.css'],
  providers: [AdminService]
})
export class RequestOppComponent implements OnInit {
  requestoppurtunities = "";
  userid;
  custompagination=true;
  customtable=true;
  P;
  nodata = false;
  searchText;
  constructor(private router: Router, private adminService: AdminService,private flashMessage: FlashMessagesService) { }



  ngOnInit() {
    this.userid = localStorage.getItem('volunteeruserid');
    if(this.userid == null){
      this.router.navigate(['/','signin']);
      return;
    }
    this.adminService.individualrequestOppurtunity(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        this.requestoppurtunities = res.content;
        $('.loading-spinner').fadeOut("slow");
      }
      else {
        $('.loading-spinner').fadeOut("slow");
        this.custompagination = false;
         this.customtable = false;
         this.nodata = true;
      }
    });
  }

}