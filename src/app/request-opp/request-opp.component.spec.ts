import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestOppComponent } from './request-opp.component';

describe('RequestOppComponent', () => {
  let component: RequestOppComponent;
  let fixture: ComponentFixture<RequestOppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestOppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestOppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
