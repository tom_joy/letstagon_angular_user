import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AdminService } from "../service/admin.service";
declare var $: any;
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css'],
	providers:[AdminService]
})
export class HeaderComponent implements OnInit {
	customlayout;
	username;
	profile;
	experiencelist;
	social_media;
	social_media_flag=false;

	constructor(private router: Router,private adminService: AdminService) { }

	ngOnInit() {



		if (localStorage.getItem('volunteeruserid') == null) {
			this.customlayout = true;
			this.username = false;
		}

		else {
			this.customlayout = false;
			this.username = true;
			this.profile = localStorage.getItem('volunteerusername');
			this.social_media = localStorage.getItem('social_media_flag');
			if(this.social_media == null){
				this.social_media_flag = true;
			}
			else{
				this.social_media_flag = false;
			}
		
		}
		

$('.show').css('height','0');
	}

	logout() {
		localStorage.removeItem('volunteeruserid');
		localStorage.removeItem('experienceid');
		localStorage.removeItem('search_details');
		localStorage.removeItem('volunteerusername');
		localStorage.removeItem('volunteerprofile_pic');
		localStorage.removeItem('exploreschedule_id');
		localStorage.removeItem('social_media_flag');
		window.location.href = "/letstagon.com";
	}
	 drop(){
    if($('#userDropDown').hasClass('activeUser')){
      $('.show').css('display','none');
      $("#userDropDown").removeClass("activeUser");
    }else{
      $("#userDropDown").addClass("activeUser");
      $('.show').css('display','block');
    }
  }

}


