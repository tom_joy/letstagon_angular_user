import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
declare var $:any;
@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.css']
})
export class DashboardHeaderComponent implements OnInit {
username;
profile;
social_media_flag;
social_media_menu = false;
  constructor(private router:Router) { }

  ngOnInit() {
    this.username = localStorage.getItem('volunteerusername');
    this.profile = localStorage.getItem('volunteerprofile_pic');
    this.social_media_flag = localStorage.getItem('social_media_flag');
    if(this.social_media_flag ==null){
      this.social_media_menu = true;
    }
      $('#dropdown').click(function(){
          if($('.dropdown-menu').hasClass('open')){
            $('.dropdown-menu').hide();
            $('.dropdown-menu').removeClass('open');
          }
          else{
            $('.dropdown-menu').removeClass('open');
            $('.dropdown-menu').addClass('open');
            $('.dropdown-menu').show();
          }
        

      });
  }
logout(){
  localStorage.removeItem('volunteeruserid');
  localStorage.removeItem('experienceid');
  localStorage.removeItem('search_details');
  localStorage.removeItem('volunteerusername');
  localStorage.removeItem('volunteerprofile_pic');
  localStorage.removeItem('exploreschedule_id');
  localStorage.removeItem('social_media_flag');
  this.router.navigate(['/letstagon.com'])
}
}
