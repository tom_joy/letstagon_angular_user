import { Component, OnInit } from '@angular/core';
import { AdminService } from "../service/admin.service";
import { AuthenticationService } from "../service/auth.service";
@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css'],
  providers: [AdminService, AuthenticationService]
})
export class TermsConditionComponent implements OnInit {

  constructor( private adminService: AdminService) { }

  ngOnInit() {

  }

}
