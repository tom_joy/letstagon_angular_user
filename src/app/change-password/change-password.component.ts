import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { AuthenticationService } from "../service/auth.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { $ } from '../../../node_modules/protractor';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  providers:[AuthenticationService]
})
export class ChangePasswordComponent implements OnInit {
  userid;
  resetForm: FormGroup;
  submitted = false;
  constructor(private route: ActivatedRoute,private router:Router,private authService:AuthenticationService,private formBuilder: FormBuilder,private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      oldpassword: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
  });
  }
  get f() { return this.resetForm.controls; }
  onSubmit() {
    this.submitted = true;

    if (this.resetForm.invalid) {
        return;
    }
    this.userid = localStorage.getItem('volunteeruserid');
    this.authService.changePassword(this.userid,this.resetForm.value.oldpassword,this.resetForm.value.password,this.resetForm.value.confirmPassword).subscribe((res: any)=>{
      if(res.status == "success"){
        this.flashMessage.show(res.message, { cssClass: 'alert-success', timeout: 8000 });
        this.ngOnInit();
        this.submitted=false;
      }
      else{
        this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 8000 });
      }
    });

  }

}
