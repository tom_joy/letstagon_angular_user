import { Component, OnInit, } from '@angular/core';
import { Router } from "@angular/router";
declare var $: any;
import { AdminService } from "../service/admin.service";
import { DataService } from "../service/data.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AdminService, DataService]
})
export class HomeComponent implements OnInit {
  experiencelist;

  message;
  city;
  category;
  searchForm: FormGroup;
  citylist;
  categorylist;
  eventcount;
  volunteercount;
  eventhours;
  groupevent;
  islogin;
  testimonialview;
  registerForm: FormGroup;
  preForm: FormGroup;
  ngoForm: FormGroup;
  coedForm: FormGroup;
  islogout;
  userid;
  scheduleid;
  submitted = false;
  location;
  locations;
  contactmessage;
  statelist;
  state_id;
  angular;
  statecitylist;
  images = ['assets/images/gallery/award.jpg', 'assets/images/gallery/bettaworkshop.jpg', 'assets/images/gallery/award1.jpg', 'assets/images/gallery/img1.jpg', 'assets/images/gallery/SLP.jpg', 'assets/images/gallery/vol.jpg', 'assets/images/gallery/sports.jpg', 'assets/images/gallery/park.jpg'];
  constructor(private router: Router, private flashMessage: FlashMessagesService, private formBuilder: FormBuilder, private adminService: AdminService, private data: DataService) { }

  ngOnInit() {
    $('.helloversha').css('display','none');
    localStorage.removeItem('search_details');
    if (localStorage.getItem('volunteeruserid') == null) {
      this.islogin = false;
      this.islogout = true;
    }
    else {
      this.islogin = true;
      this.islogout = false;
    }
    this.registerForm = this.formBuilder.group({
      organization: ['', [Validators.required]],
      volunteers: ['', [Validators.required]],
      description: ['', [Validators.required]],
      date: ['', [Validators.required]],
      location:['',[Validators.required]]
    });
    this.adminService.homeexperiencelist().subscribe((res: any) => {
      if (res.status == "success") {
        this.experiencelist = res.content;
        console.log(this.experiencelist)
        $('.loading-spinner').hide();
      }
      else {
        $('.loading-spinner').hide();
      }
    });

    // testimonial
    this.adminService.viewtestimonial().subscribe((res: any) => {
      if (res.status == "success") {
        this.testimonialview = res.data;
      
        $('.loading-spinner').hide();
      }
      else {
        $('.loading-spinner').hide();
      }
    });
    // testimonial
    $('#button-Modal').click(function () {
      $('.Modal').show();
    });
    $('.close').click(function () {
      $('.Modal').hide();
    });
    this.adminService.getCities().subscribe((res: any) => {
      if (res.status == "success") {
        this.citylist = res.data;
        $('.loading-spinner').hide();
      }
      else {
        $('.loading-spinner').hide();
      }
    });
    this.adminService.Counters().subscribe((res: any) => {
      if (res.status == "success") {
        this.eventcount = res.data.event_count;
        this.volunteercount = res.data.volunteer_count;
        this.eventhours = res.data.event_hours;
        this.groupevent = res.data.group_count;
      }
      else {

      }

    });
    this.preForm = this.formBuilder.group({
      location: ['',[Validators.required]]
    });
    this.searchForm = this.formBuilder.group({
      events: [''],
      city: [''],
      category: [''],
    });

    this.ngoForm = this.formBuilder.group({
      ngo_name: ['', [Validators.required]],
      ngo_type: ['', [Validators.required]],
      ngo_description: ['', [Validators.required]],
      contact_number: ['', [Validators.required, Validators.pattern(/^(\+|\d)[0-9]{7,16}$/)]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      contact_person: ['', [Validators.required]],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required]

    });

    this.coedForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      type: ['', [Validators.required]],
      description: ['', [Validators.required]],
      contact_number:['', [Validators.required, Validators.pattern(/^(\+|\d)[0-9]{7,16}$/)]],
      contact_person: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],

    });

    this.adminService.getstatelist().subscribe((res: any) => {
      if (res.status == "success") {
          this.statelist = res.data;
          $('.loading-spinner').hide();
      }
      else {
          $('.loading-spinner').hide();
          alert(res.message);
      }
  });


  }
  get f() { return this.registerForm.controls; }
  get g() { return this.preForm.controls; }
  get n() { return this.ngoForm.controls; }
  get c() { return this.coedForm.controls; }
  
  opendetailedpage(experience) {
    localStorage.removeItem('experienceid');
    localStorage.setItem('experienceid', experience.experienceid);
    this.router.navigate(['/', 'opportunitydetail']);
  }

  Search() {
    localStorage.setItem('search_details', JSON.stringify(this.searchForm.value));
    this.router.navigate(['/', 'explore']);
  }
  scheduleButton(events) {
    localStorage.removeItem('homeschedule_id');
    localStorage.setItem('homeschedule_id', events.id);
    $('#Modalschedule').css('display', 'block');
  }
  sendtologin() {
    this.router.navigate(['/', 'signin']);
  }
  modelDissmiss() {
    $('#Modalschedule').css('display', 'none');
  }
  category_section(category) {

    this.categorylist = { "events": "", "city": "", "category": category };
    localStorage.setItem('search_details', JSON.stringify(this.categorylist));
    this.router.navigate(['/', 'explore']);
  }

  ngoSend() {
    $('#Modalngo').css('display', 'block');
    $('section.discrool').css('position','fixed');
  }

  sendcoed(){
    $('#Modalcoed').css('display', 'block'); 
  }

  ngoSend1(num) {
    $('#Modalgallery').css('display', 'block');
    $(".carousel-item").removeClass('active');
    $('.carousel-item:eq('+num+')').addClass("active");
  }
  onSubmit() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.submitted = true;

      return;
    }
    this.userid = localStorage.getItem('volunteeruserid');
    this.scheduleid = localStorage.getItem('homeschedule_id');
    this.adminService.Customrequest(this.userid, this.scheduleid, this.registerForm.value.organization, this.registerForm.value.volunteers, this.registerForm.value.description, this.registerForm.value.date,this.registerForm.value.location).subscribe((res: any) => {
      if (res.status == "success") {
        this.flashMessage.show(res.message, { cssClass: 'alert-success', timeout: 5000 });
        this.ngOnInit();
        this.submitted = false;
      }
      else {

        this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 5000 });
        this.submitted = false;
        this.ngOnInit();

      }
    });
  }

  
  onStateSelect(event) {
    this.state_id = event.target.value;
    this.adminService.getcitybystate(this.state_id).subscribe((res: any) => {
        if (res.status == "success") {
            this.statecitylist = res.content;
            // console.log(this.statecitylist);
        }
        else{
            console.log(res.message);
        }
    });
}

  ngoSubmit() {
    //this.submitted = true;
    $('.loading-spinner').show();
    // stop here if form is invalid
    if (this.ngoForm.invalid) {
      this.submitted = true;
      $('.loading-spinner').hide();
      return;
    }
    this.adminService.ngoinsert(this.ngoForm.value).subscribe((res: any) => {
      
      if (res.status == "success") {
       
        this.contactmessage = true;
        this.submitted = false;
        this.message = res.message;
        this.ngoForm.reset();
        $('.loading-spinner').hide();
      }
      else { 
        this.contactmessage = true;
        this.message = res.message;
        this.submitted = false;
        this.ngoForm.reset();
        
      }
  });
  }


  coedSubmit() {
    //this.submitted = true;
    $('.loading-spinner').show();
    // stop here if form is invalid
    if (this.coedForm.invalid) {
      this.submitted = true;
      $('.loading-spinner').hide();
      return;
    }
    this.adminService.coedsend(this.coedForm.value).subscribe((res: any) => {
     
      if (res.status == "success") {
       
        this.contactmessage = true;
        this.submitted = false;
        this.message = res.message;
        this.coedForm.reset();
        $('.loading-spinner').hide();
      }
      else { 
        this.contactmessage = true;
        this.message = res.message;
        this.submitted = false;
        this.coedForm.reset();
        
      }
  });
  }

  tagon() {
    $('.loading-spinner').show();
	this.userid = localStorage.getItem('volunteeruserid');
	if (this.preForm.invalid) {
		this.submitted = true;
		$('.loading-spinner').hide();
		return;
	  }
    this.adminService.tagon(this.preForm.value.location, this.userid).subscribe((res: any) => {
      if (res.status == "success") {
		this.contactmessage = true;
		this.submitted = false;
        this.message = res.message;
        this.preForm.reset();
        $('.loading-spinner').hide();
      }
      else {
        this.contactmessage = true;
		this.message = res.message;
		this.submitted = false;
        this.preForm.reset();
        $('.loading-spinner').hide();
      }
    });
  }
  knowmore(experience){
    localStorage.removeItem('experienceid');
    localStorage.setItem('experienceid', experience.experienceid);
    localStorage.setItem('eventid', experience.id);
    this.router.navigate(['/','opportunitydetail']);
  }
  close() {
    this.contactmessage = false;
    $('#Modallocation').css('display', 'none');
  }
  locationButton(events) {
    this.userid = localStorage.getItem('volunteeruserid');
    this.adminService.locations(this.userid, events.experienceid).subscribe((res: any) => {
   
      if (res.status == "success") {
       
        this.location = res.data;
        $('.loading-spinner').hide();
        this.locations = res.data;
      }
      else{
       
       $('.btn').css('display','none');
        $("#message").html("You have already applied for this opportunity");
       $('.loading-spinner').hide();
      }
    });
    $('#Modallocation').css('display', 'block');
  }
  modelClose() {
	this.submitted = false;
	this.preForm.reset();
	$('#Modallocation').css('display', 'none');
}
modelcancle() {
  this.submitted = false;
  this.ngoForm.reset();
  $('#Modalngo').css('display', 'none');
  $('section.discrool').css('position','relative');
}
modelend(){
  this.submitted = false;
  this.coedForm.reset();
  $('#Modalcoed').css('display', 'none');
  $('#Modalgallery').css('display','none');
}

onRatingSet(experience){
  localStorage.setItem('event_id',experience.id);
localStorage.setItem('experience_id',experience.experienceid);
localStorage.setItem('user_taggedid',this.userid);
localStorage.setItem('homepage','true');
console.log(experience);
  this.router.navigate(['/','addreview']);
}
}
