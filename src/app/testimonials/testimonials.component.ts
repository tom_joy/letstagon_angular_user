import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css'],
  providers:[AdminService]
})
export class TestimonialsComponent implements OnInit {
userid;
firstname;
lastname;
message;
subject;
fileurl;
designation;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {

    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.adminService.viewTestimonial(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        this.firstname = res.data.first_name;
        this.lastname = res.data.last_name;
        this.message = res.data.message;
        this.subject = res.data.subject;
        this.fileurl = res.data.image_path;
        this.designation = res.data.designation;
      }
      else {
        $('.loading-spinner').fadeOut('slow');
            alert(res.message);
      }

    });

  }

}
