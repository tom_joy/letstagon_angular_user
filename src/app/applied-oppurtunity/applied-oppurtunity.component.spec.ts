import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedOppurtunityComponent } from './applied-oppurtunity.component';

describe('AppliedOppurtunityComponent', () => {
  let component: AppliedOppurtunityComponent;
  let fixture: ComponentFixture<AppliedOppurtunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedOppurtunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedOppurtunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
