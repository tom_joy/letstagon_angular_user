import { Component, OnInit } from '@angular/core';
import { AdminService } from "../service/admin.service";
import { Router } from "@angular/router";
import { FlashMessagesService } from 'angular2-flash-messages';
declare var $: any;
@Component({
  selector: 'app-applied-oppurtunity',
  templateUrl: './applied-oppurtunity.component.html',
  styleUrls: ['./applied-oppurtunity.component.css'],
  providers: [AdminService]
})
export class AppliedOppurtunityComponent implements OnInit {
  appliedoppurtunities = "";
  userid;
  custompagination = true;
  customtable = true;
  P;
  nodata = false;
  searchText;
  constructor(private router: Router, private adminService: AdminService, private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.adminService.individualOppurtunity(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        this.appliedoppurtunities = res.content;
        $('.loading-spinner').fadeOut("slow");
      }
      else {
        $('.loading-spinner').fadeOut("slow");
        this.custompagination = false;
        this.customtable = false;
        this.nodata = true;
      }
    });
  }
  review(experience){
    localStorage.setItem('event_id',experience.event_id);
	localStorage.setItem('experience_id',experience.experience_id);
	localStorage.setItem('user_taggedid',experience.user_tag_id);
  localStorage.setItem('homepage','false');
    this.router.navigate(['/','addreview']);
  }


}
