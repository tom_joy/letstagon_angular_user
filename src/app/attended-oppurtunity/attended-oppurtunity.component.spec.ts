import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendedOppurtunityComponent } from './attended-oppurtunity.component';

describe('AttendedOppurtunityComponent', () => {
  let component: AttendedOppurtunityComponent;
  let fixture: ComponentFixture<AttendedOppurtunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendedOppurtunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendedOppurtunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
