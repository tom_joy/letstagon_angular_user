import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { AuthenticationService } from "../service/auth.service";
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService, SocialUser, SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from "ng4-social-login";
@Component({
	selector: 'app-signin',
	templateUrl: './signin.component.html',
	styleUrls: ['./signin.component.css'],
	providers: [AuthenticationService]
})
export class SigninComponent implements OnInit {
	signinForm: FormGroup;
	submitted = false;
	resetForm: FormGroup;
	emailVaildation;
	emailexists;
	public user: any = SocialUser;

	constructor(private formBuilder: FormBuilder, private router: Router, private flashMessage: FlashMessagesService, private authServices: AuthenticationService, private socialAuthService: AuthService) { }

	socialSignIn(socialPlatform: string) {

		let socialPlatformProvider;
		if (socialPlatform == "google") {
			socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
		}
		else if (socialPlatform == "linkedin") {

			socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
		}

		this.socialAuthService.signIn(socialPlatformProvider).then(
			(userData) => {
				console.log(userData);
				if (userData) {
					this.authServices.sociallogin(userData.id, userData.name, userData.email).subscribe((res: any) => {
						if (res.status == "success") {
							localStorage.setItem('volunteeruserid', res.data.user_id);
							localStorage.setItem('volunteerusername', res.data.user_name);
							localStorage.setItem('volunteerprofile_pic', res.data.image_path);
							localStorage.setItem('social_media_flag', res.data.social_media_flag);
							this.router.navigate(['/', 'home']);
						}
					});
				}
			}
		);
	}
	ngOnInit() {

		if (localStorage.getItem('activateid') != null) {
			this.flashMessage.show("Sucessfully Activated", { cssClass: 'alert-success', timeout: 3000 });
		}
		if (localStorage.getItem('notactivated') != null) {
			this.flashMessage.show(localStorage.getItem('notactivated'), { cssClass: 'alert-danger', timeout: 3000 });
		}
		$('.loading-spinner').hide();
		this.signinForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.email]],
			password: ['', [Validators.required, Validators.minLength(6)]]
		});
		this.resetForm = this.formBuilder.group({
			email1: ['', [Validators.required, Validators.email]]
		});


		$('#button-Modal').click(function () {
			$('#myModal').show();
		});
		$('.close').click(function () {
			$('#myModal').hide();
		});
	}
	// convenience getter for easy access to form fields
	get f() { return this.signinForm.controls; }
	get g() { return this.resetForm.controls; }



	resetPassword(email) {

		this.emailVaildation = false;
		this.emailexists = false;
		if (email == null || email == "") {
			this.emailVaildation = true;
		}
		if (this.resetForm.invalid) {
			return;
		}
		this.emailVaildation = false;
		$('.loading-spinner').show();
		this.authServices.forgotPassword(email).subscribe((res: any) => {
			if (res.status == "success") {
				$('.loading-spinner').fadeOut('slow');
				$('#myModal').hide();
				this.flashMessage.show(res.message, { cssClass: 'alert-success', timeout: 2000 });
			}
			else {
				$('.loading-spinner').hide();
				this.emailexists = true;

			}
		});

	}
	onSubmit() {
		this.submitted = true;
		$('.loading-spinner').show();
		// stop here if form is invalid
		if (this.signinForm.invalid) {
			$('.loading-spinner').hide();
			return;
		}
		this.authServices.checklogin(this.signinForm.value.email, this.signinForm.value.password).subscribe((res: any) => {
			if (res.status == "success") {
				localStorage.setItem('volunteeruserid', res.data.user_id);
				localStorage.setItem('volunteerusername', res.data.user_name);
				localStorage.setItem('volunteerprofile_pic', res.data.image_path);
				localStorage.setItem('auth_key', res.data.auth_key);
				$('.loading-spinner').hide();
				localStorage.removeItem('activateid');
				localStorage.removeItem('notactivated');
				this.flashMessage.show(res.message, { cssClass: 'alert-success', timeout: 2000 });
				//Checking whether experience id exists
				if (localStorage.getItem('experienceid') != null) {
					this.router.navigate(['/', 'opportunitydetail']);
				}
				else if (localStorage.getItem('exploreschedule_id') != null) {
					this.router.navigate(['/', 'explore']);
				}
				else {
				
					// this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 2000 });
					 this.router.navigate(['/home']);
				}

			}
			else {

				$('.loading-spinner').hide();
				this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 2000 });
			
			}
		});


	}
}
