import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../service/admin.service';
import { FlashMessagesService } from 'angular2-flash-messages';
declare var $: any;
@Component({
  selector: 'app-detail-description',
  templateUrl: './detail-description.component.html',
  styleUrls: ['./detail-description.component.css'],
  providers:[AdminService]
})
export class DetailDescriptionComponent implements OnInit {

	experiencedetails;
	eventdetails;
	experience_name;
	cause_name;
	category_name;
	description_details;
	image_path;
	expid;
	userid;
	experiencelist;
	isLoaded;
	islogin;
	islogout;
	contactmessage;
	message;
	schedule_type;
	reviewdetails;
	P;
	experiencedetail;
	experienceid;
	event_schedule;
	scheduleid;
	event_type;
	ngo_name;
	participation_type;
	location;
	locations;
	registerForm: FormGroup;
	preForm: FormGroup;
	submitted = false;
	custompagination = false;
	ngo_description;
	Custom_Schedule = false;
	Pre_Schedule = false;
	exp;
	schedule_id;
	constructor(private router: Router, private route: ActivatedRoute,private adminService: AdminService, private flashMessage: FlashMessagesService, private formBuilder: FormBuilder) { }

	ngOnInit() {

		this.route.params.subscribe(params => {

				this.expid = params['id'];
		
  		$('.loading-spinner').show();
	
			localStorage.setItem('experienceid', this.expid);
		if (localStorage.getItem('volunteeruserid') == null) {

			
			this.islogin = false;
			this.islogout = true;
		}
		else {
			this.islogin = true;
			this.islogout = false;
		
			$("#loginmessage").html("Sorry... You already applied for this opportunity");
		}

		this.preForm = this.formBuilder.group({
			location: ['', [Validators.required]]
		});
		this.registerForm = this.formBuilder.group({
			organization: ['', [Validators.required]],
			volunteers: ['', [Validators.required]],
			location: ['', [Validators.required]],
			description: ['', [Validators.required]],
			date: ['', [Validators.required]],
		});
		this.adminService.opendetailedexperience(this.expid).subscribe((res: any) => {
		
			if (res.status == "success") {
			
				$('.loading-spinner').fadeOut("slow");
				this.experience_name = res.experience_name;
				this.cause_name = res.cause;
				this.category_name = res.category;
				this.description_details = res.description;
				this.image_path = res.image_path;
				this.ngo_name =res.ngo_name;
				this.ngo_description = res.ngo_description;
				this.participation_type = res.participation_type;
				this.event_type = res.event_type;
				this.event_schedule = res.event_schedule;
				this.isLoaded = true;
				this.experienceid = res.experience_id;
				if (res.event_schedule == "Custom-Schedule") {
					this.Custom_Schedule = true;
				}
				else {
					this.Pre_Schedule = true;
				}

			}
			else {
				$('.loading-spinner').fadeOut("slow");
				alert(res.message);
			}

		});

		$('#button-Modal').click(function () {
			$('.Modal').show();
		});
		$('.close').click(function () {
			$('.Modal').hide();
		});
		$('#Modalschedule').css('display', 'none');

		this.adminService.Reviewlists(this.expid).subscribe((res: any) => {
			if (res.status == "success") {
				this.reviewdetails = res.data;
				this.custompagination = true;
			}
			else {
				this.custompagination = false;
			}
		});
		this.schedule_type = localStorage.getItem('schedule_type');
		
		// if(this.schedule_type != 'Pre-Schedule'){
		// 	console.log(this.schedule_type);
		// 	$('#Modalschedule').css('display','block');
		// 	}
		// 	else{
		// 		$('#Modalschedule').css('display','none');
		// 	}
	});
	}
	// convenience getter for easy access to form fields
	get g() { return this.preForm.controls; }
	get f() { return this.registerForm.controls; }



	tagon(experience) {
		
		 
		$('.loading-spinner').show();
		if (localStorage.getItem('volunteeruserid') == null) {
		
			this.router.navigate(['/', 'signin']);
		}
		else {
			this.router.navigate(['/', 'detail-description/:id']);
			this.userid = localStorage.getItem('volunteeruserid');
			this.adminService.tagon(experience.id, this.userid).subscribe((res: any) => {
				if (res.status == "success") {
					$('.loading-spinner').hide();
					this.flashMessage.show("Thank you for applying to the opportunity. LetsTagOn Team will soon get in touch with you.", { cssClass: 'alert-success', timeout: 8000 });
				}
				else {
					$('.loading-spinner').hide();
					this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 8000 });
				}

			});
		}

	}
	scheduleButton(events) {
		localStorage.removeItem('schedule_id');
		localStorage.setItem('schedule_id', events.id);
		this.submitted = false;
		$('#Modalschedule').css('display', 'block');
	}

	
	sendtologin() {
		
		this.router.navigate(['/', 'signin']);

		this.route.params.subscribe(params => {
		this.expid = params['id'];
		localStorage.setItem('experienceid', this.expid);
		
		});
	}

	modelDissmiss() {
		$('#Modalschedule').css('display', 'none');

	}



	onSubmit() {

		// stop here if form is invalid
		if (this.registerForm.invalid) {
			this.submitted = true;

			return;
		}
		this.userid = localStorage.getItem('volunteeruserid');
		this.scheduleid = localStorage.getItem('eventid');
		// console.log(this.scheduleid);
		this.adminService.Customrequest(this.userid, this.scheduleid, this.registerForm.value.organization, this.registerForm.value.volunteers, this.registerForm.value.description, this.registerForm.value.date, this.registerForm.value.location).subscribe((res: any) => {
			if (res.status == "success") {
				this.flashMessage.show(res.message, { cssClass: 'alert-success', timeout: 5000 });
				this.registerForm.reset();
				this.submitted = false;
			}
			else {

				this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 5000 });
				this.registerForm.reset();
				this.submitted = false;

			}
		});
	}
	firsttab() {

		$('#menu2').removeClass('in');
		$('#menu2').removeClass('active');
		$('#menu1').addClass('in');
		$('#menu1').addClass('active');
	}
	secondtab() {
		$('#menu1').removeClass('in');
		$('#menu1').removeClass('active');
		$('#menu2').addClass('in');
		$('#menu2').addClass('active');
	}

	tagnow() {
		$('.loading-spinner').show();
		this.userid = localStorage.getItem('volunteeruserid');
		
	
		if (this.preForm.invalid) {
			this.submitted = true;
			$('.loading-spinner').hide();
		return;
		}

		this.adminService.tagon(this.preForm.value.location, this.userid).subscribe((res: any) => {
			if (res.status == "success") {
				this.contactmessage = true;
				this.message = res.message;
				this.submitted = false;
				this.preForm.reset();
				$('.loading-spinner').hide();
	
			}
			else {
				this.contactmessage = true;
				this.message = res.message;
				this.preForm.reset();
				this.submitted = false;
				$('.loading-spinner').hide();
			}
		});
	}
	close() {
		this.contactmessage = false;
		$('#Locationmodal').css('display', 'none');
	}

	tagnowButton(events) {
		// console.log(events);
		this.userid = localStorage.getItem('volunteeruserid');
	
		this.adminService.locations(this.userid, this.expid).subscribe((res: any) => {
			
			if (res.status == "success") {
				this.location = res.data;
				$('.loading-spinner').hide();
				this.locations = res.data;
			}
			else {
				(res.status == "fail")
                $('.btn').css('display','none');
                $("#message").html("Sorry... You already applied for this opportunity");
				$('.loading-spinner').hide();
			}
		});
		$('#Locationmodal').css('display', 'block');
	}
	modelClose() {
		$('#Locationmodal').css('display', 'none');
	}
}
