import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-addreview',
  templateUrl: './addreview.component.html',
  styleUrls: ['./addreview.component.css'],
  providers: [AdminService]
})
export class AddreviewComponent implements OnInit {
  userid;
  eventid;
  addReview:FormGroup;
  submitted = false;
  experienceid;
  experiencename;
  location;
  date;
  user_tagged_id;
  star_rating = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
	$('.loading-spinner').show();
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
	}
  this.eventid = localStorage.getItem('event_id');
  this.experienceid = localStorage.getItem('experience_id');
  this.user_tagged_id = localStorage.getItem('user_taggedid');
    this.adminService.getEvent(this.eventid,this.userid).subscribe((res: any) => {
      if (res.status == "success") {
		$('.loading-spinner').fadeOut('slow');
		this.router.navigate(['/','reviews']);
      }
      else {
		this.adminService.getExperiencedetails(this.eventid).subscribe((res: any) => {
			
			if (res.status == "success") {
				$('.loading-spinner').fadeOut('slow');
				this.experiencename = res.data[0].experience_name;
				this.location = res.data[0].address;
				this.date = res.data[0].date_scheduled;
			  }
			  else{
				$('.loading-spinner').fadeOut('slow');
			  }
		});
        $('.loading-spinner').fadeOut('slow');
      }

	});
	
	this.addReview = this.formBuilder.group({
		message: ['', [Validators.required]],
		subject: ['', [Validators.required]],
		rating: ['', [Validators.required]],
	  });
  
  }

get f() { return this.addReview.controls; }

onSubmit() {
    $('.loading-spinner').show();
    this.submitted=true;
    // stop here if form is invalid
    if (this.addReview.invalid) {
	  $('.loading-spinner').fadeOut('slow');
    this.submitted=true;
   if(this.addReview.value.rating == "" ||this.addReview.value.rating == null ){
     this.star_rating = true;

   }
      return;
    }
    this.adminService.addReview(this.addReview.value.subject,this.addReview.value.message,this.addReview.value.rating,this.userid,this.eventid,this.experienceid).subscribe((res: any) => {
      if ((res.status == "success") && (localStorage.getItem('homepage') !== 'true'))  {
        this.adminService.approveAttendance(this.user_tagged_id, this.userid).subscribe((res: any) => {
          if (res.status == "success") {
            this.star_rating = false;
            $('.loading-spinner').fadeOut("slow");
          
            this.router.navigate(['/','attendedoppurtunity']);
          }
          else {
            $('.loading-spinner').fadeOut("slow");
           
          }
        });
    
      }else if ((res.status == "success") && (localStorage.getItem('homepage') == 'true'))  {
        this.router.navigate(['/','home']);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
  }
}
