import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { OpportunitydetailComponent } from './opportunitydetail/opportunitydetail.component';
import { MailActivationComponent} from './mail-activation/mail-activation.component';
import { ExploreComponent } from './explore/explore.component';
import { OnlyfooterComponent } from './onlyfooter/onlyfooter.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AppliedOppurtunityComponent} from './applied-oppurtunity/applied-oppurtunity.component';
import { AttendedOppurtunityComponent } from './attended-oppurtunity/attended-oppurtunity.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { EditTestimonialComponent } from './edit-testimonial/edit-testimonial.component';
import { AddTestinomialComponent } from './add-testinomial/add-testinomial.component';
import { ReviewComponent } from './review/review.component';
import { AddreviewComponent } from './addreview/addreview.component';
import { EditReviewComponent } from './edit-review/edit-review.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { RequestOppComponent } from './request-opp/request-opp.component';
import { DetailDescriptionComponent } from './detail-description/detail-description.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
const routes: Routes = [
  
  { path: 'header', component: HeaderComponent },
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'opportunitydetail', component: OpportunitydetailComponent },
  {path: 'mail-activation/:id',component:MailActivationComponent},
  { path: 'explore', component: ExploreComponent },
  { path: 'onlyfooter', component: OnlyfooterComponent},
  {path:'reset-password/:id',component:ResetPasswordComponent},
  {path:'editprofile',component:UserProfileComponent},
  {path:'dashboard',component:DashboardComponent},
  {path:'appliedoppurtunity',component:AppliedOppurtunityComponent},
  {path:'attendedoppurtunity',component:AttendedOppurtunityComponent},
  {path:'profile',component:ProfileComponent},
  {path:'change-password',component:ChangePasswordComponent},
  {path:'testimonial',component:TestimonialsComponent},
  {path:'edit-testimonial',component:EditTestimonialComponent},
  {path:'add-testimonial',component:AddTestinomialComponent},
  {path:'reviews',component:ReviewComponent},
  {path:'addreview',component:AddreviewComponent},
  {path:'edit-review',component:EditReviewComponent},
  {path:'aboutus', component:AboutusComponent},
  {path:'request-opp', component:RequestOppComponent},
  {path:'detail-description/:id',component:DetailDescriptionComponent},
  {path:'terms-condition',component:TermsConditionComponent},
  {path:'privacy-policy',component:PrivacyPolicyComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
