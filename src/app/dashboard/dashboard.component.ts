import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
declare var $: any;
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [AdminService]
})
export class DashboardComponent implements OnInit {
  userid;
  constructor(private router: Router, private adminService: AdminService) { }
  appliedopportunity;
  attendedopportunity;
  totalopportunity;
  event_hours;

  ngOnInit() {
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.adminService.dashboardCount(this.userid).subscribe((res: any) => {
      if (res.status == "success") {

        this.appliedopportunity = res.data.applied_opportunities;
        this.attendedopportunity = res.data.attended_opportunities;
        this.totalopportunity = res.data.total_opportunities;
        this.event_hours = res.data.event_hours;

      }

    });
  }

}
