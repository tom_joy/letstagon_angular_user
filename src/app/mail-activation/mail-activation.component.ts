import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { AuthenticationService } from "../service/auth.service";
@Component({
  selector: 'app-mail-activation',
  templateUrl: './mail-activation.component.html',
  styleUrls: ['./mail-activation.component.css'],
  providers:[AuthenticationService]
})
export class MailActivationComponent implements OnInit {
  userid;
  constructor(private route: ActivatedRoute,private authService:AuthenticationService,private router:Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userid = params['id'];
      this.authService.userActivation(this.userid).subscribe((res: any) => {
				if (res.status == "success") {
          localStorage.removeItem('notactivated');
          localStorage.setItem('activateid',"activated");
					this.router.navigate(['/','signin']);	
				}
				else {
				localStorage.removeItem('activateid');
          localStorage.setItem('notactivated',res.message);
          this.router.navigate(['/','signin']);	
				}

			});
    });
  }

}
