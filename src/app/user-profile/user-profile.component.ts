import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
declare var $: any;
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from '../service/admin.service';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [AdminService]
})
export class UserProfileComponent implements OnInit {
  editprofile: FormGroup;
  submitted = false;
  userid;
  dob;
  fileurl;
  
  file: File;
  fd;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    $('.loading-spinner').show();
    this.userid = localStorage.getItem('volunteeruserid');
   
    if (this.userid == null) {
      this.router.navigate(['/', 'signin']);
      return;
    }
    this.userid = localStorage.getItem('volunteeruserid');
    this.editprofile = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: [''],
      phone: ['', [Validators.required, Validators.pattern(/^(\+|\d)[0-9]{7,16}$/)]],
      organization: [''],
   
      designation: [''],
      email: ['', [Validators.required]],
      user_id: [''],
      dob: [''],
      skills:[''],
      interests:[''],
      file: ['', Validators.required],
    });
    this.adminService.getProfile(this.userid).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
          this.dob =res.data.dob;
          this.fileurl = res.data.file;
        this.editprofile.setValue(res.data);
      }
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }

    });
    
    

    
  
  }
  get f() { return this.editprofile.controls; }

  fileChange(files: any) {
    this.file = files[0];
  }

  onSubmit() {

    // $('.loading-spinner').show();
    // stop here if form is invalid
    if (this.editprofile.invalid) {
      this.submitted = true;
      
      $('.loading-spinner').hide();
      $('.loading-spinner').fadeOut('slow');
      return;
  }
  this.fd = new FormData();
  this.fd.append('user_id', this.userid);
  this.fd.append('first_name', this.editprofile.value.first_name);
  this.fd.append('last_name', this.editprofile.value.last_name);
  this.fd.append('organization', this.editprofile.value.organization);
  this.fd.append('file', this.file);
  this.fd.append('phone', this.editprofile.value.phone);
  this.fd.append('email', this.editprofile.value.email);
  this.fd.append('designation', this.editprofile.value.designation);
 
  var curDate = new Date();

   
    if(new Date(this.editprofile.value.dob) > curDate){
      this._flashMessagesService.show("Please Enter Valid Date", { cssClass: 'alert-danger', timeout: 3000 });
   
    }else{
      this.fd.append('dob', this.editprofile.value.dob);
    }
  this.fd.append('skills', this.editprofile.value.skills);
  this.fd.append('interests', this.editprofile.value.interests);
    this.adminService.updateProfile(this.fd).subscribe((res: any) => {
      if (res.status == "success") {
        $('.loading-spinner').fadeOut('slow');
        localStorage.setItem('volunteerusername', res.data.user_name);
				localStorage.setItem('volunteerprofile_pic', res.data.image_path);
        this._flashMessagesService.show(res.message, { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/', 'profile']);
      }
      
      else {
        $('.loading-spinner').fadeOut('slow');
        this._flashMessagesService.show(res.message, { cssClass: 'alert-danger', timeout: 3000 });
      }
    });

  }

}
