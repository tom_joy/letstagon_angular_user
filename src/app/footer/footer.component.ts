import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { FlashMessagesService } from 'angular2-flash-messages';
import { AdminService } from "../service/admin.service";

declare var $:any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class FooterComponent implements OnInit {
  homeForm: FormGroup;
  submitted = false;
  messageService;
  contactmessage = false;
  contactsuccess;
  cars;

  images = ['assets/images/clients/Elevate.jpg', 'assets/images/clients/goldman.png', 'assets/images/clients/NSRCEL.jpg', 'assets/images/clients/startup.png', 'assets/images/clients/WeSchool.jpg'];
  constructor(private formBuilder: FormBuilder, private router: Router, private flashMessage: FlashMessagesService,private adminService:AdminService) {
    this.cars = [
      {brand: 'Elevate'},
     
      {brand: 'NSRCEL'},
      {brand: 'startup'},
      {brand: 'WeSchool'},
      {brand: 'goldman'},
     
  ];
   }

  ngOnInit() {
    $('.loading-spinner').hide();
    this.homeForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      message: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern(/^[6-9]\d{9}$/)]],
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.homeForm.controls; }

  onSubmit() {
    this.submitted = true;
    $('.loading-spinner').show();
    // stop here if form is invalid
    if (this.homeForm.invalid) {
      $('.loading-spinner').hide();
      return;
    }
    this.adminService.Sendcontactmail(this.homeForm.value).subscribe((res: any) => {
        if(res.status=="success"){
          $('.loading-spinner').hide();
          this.ngOnInit();
          this.submitted = false;
          this.contactmessage = true;
        }
    });





  }
  close(){
    this.contactmessage = false;
  }

  

}
