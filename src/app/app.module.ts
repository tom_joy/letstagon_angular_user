import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { OpportunitydetailComponent } from './opportunitydetail/opportunitydetail.component';
import { HttpClientModule } from '@angular/common/http';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { MailActivationComponent } from './mail-activation/mail-activation.component';
import { ExploreComponent } from './explore/explore.component';
import { OnlyfooterComponent } from './onlyfooter/onlyfooter.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { DashboardHeaderComponent } from './dashboard-header/dashboard-header.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GrdFilterPipe } from './grd-filter.pipe';
import { FormsModule } from '@angular/forms';
import { AppliedOppurtunityComponent } from './applied-oppurtunity/applied-oppurtunity.component';
import { AttendedOppurtunityComponent } from './attended-oppurtunity/attended-oppurtunity.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { EditTestimonialComponent } from './edit-testimonial/edit-testimonial.component';
import { AddTestinomialComponent } from './add-testinomial/add-testinomial.component';
import { ReviewComponent } from './review/review.component';
import { AddreviewComponent } from './addreview/addreview.component';
import { EditReviewComponent } from './edit-review/edit-review.component'; 
import { NgxInputStarRatingModule } from 'ngx-input-star-rating';
import { MatDatepickerModule , MatFormFieldModule ,MatNativeDateModule} from '@angular/material';
import { AboutusComponent } from './aboutus/aboutus.component';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider,LinkedinLoginProvider} from "ng4-social-login";
import { RequestOppComponent } from './request-opp/request-opp.component';
import { NgxStarsModule } from 'ngx-stars';
import { DataService } from './service/data.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// import {CarouselModule} from 'primeng/carousel';
// import {ToastModule} from 'primeng/toast';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { DetailDescriptionComponent } from './detail-description/detail-description.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';




let config = new AuthServiceConfig([
 
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("638429484209-7unmgqj3ei53p0al6q4bko5os781fr3u.apps.googleusercontent.com")
  },
  {
    id: LinkedinLoginProvider.PROVIDER_ID,
    provider: new LinkedinLoginProvider("812hohjwg2oe4s")
  }
],false);
 
export function provideConfig() {
  return config;
}
@NgModule({
  declarations: [
   
    AppComponent,
 
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    SigninComponent,
    SignupComponent,
    OpportunitydetailComponent,
    MailActivationComponent,
    ExploreComponent,
    OnlyfooterComponent,
    ResetPasswordComponent,
    DashboardHeaderComponent,
    UserProfileComponent,
    DashboardComponent,
    GrdFilterPipe,
    AppliedOppurtunityComponent,
    AttendedOppurtunityComponent,
    ProfileComponent,
    ChangePasswordComponent,
    TestimonialsComponent,
    EditTestimonialComponent,
    AddTestinomialComponent,
    ReviewComponent,
    AddreviewComponent,
    EditReviewComponent,
    AboutusComponent,
    RequestOppComponent,
    DetailDescriptionComponent,
    TermsConditionComponent,
    PrivacyPolicyComponent
  
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    NgxStarsModule,
    // CarouselModule,
    // ToastModule,
    InternationalPhoneNumberModule,
    NgxPaginationModule,
    NgxInputStarRatingModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    SocialLoginModule,
    FlashMessagesModule.forRoot(),
  ],
  providers: [
   DataService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
