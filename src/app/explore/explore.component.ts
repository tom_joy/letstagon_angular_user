import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
declare var $: any;
import { AdminService } from "../service/admin.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from "../service/data.service";
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.css'],
  providers: [AdminService,DataService]
})
export class ExploreComponent implements OnInit {

  experiencelist;
  isLoaded;
  citylist;
  message;
  searchdetails;
  searchForm:FormGroup;
  checklist;
  checkingevents;
  p;
  islogin;
  islogout;
  registerForm: FormGroup;
  preForm:FormGroup;
  userid;
  scheduleid;
  contactmessage;
  locations;
  submitted = false;
  location;
  experienceid;
  category_name;
  id;
  city;
  city_id;
  events;
  category;
  SearchCategory;
  event;
  event_id;

  constructor(private router: Router, private flashMessage: FlashMessagesService,private adminService: AdminService,private formBuilder: FormBuilder,private data: DataService) { }

  ngOnInit() {
    this.isLoaded = false;

    this.data.currentMessage.subscribe(message => this.message = message)
    this.searchdetails = JSON.parse(localStorage.getItem('search_details'));
    if(localStorage.getItem('volunteeruserid') == null){
      this.islogin = false;
      this.islogout = true;
    }
    else{
      this.islogin = true;
      this.islogout = false;
    }

    this.preForm = this.formBuilder.group({
      location: ['',[Validators.required]]
    });
    this.registerForm = this.formBuilder.group({
      organization: ['', [Validators.required]],
      volunteers: ['', [Validators.required]],
      description: ['', [Validators.required]],
      date:['',[Validators.required]],
      location:['',[Validators.required]],
    });
    this.adminService.getFilteredlist(this.searchdetails).subscribe((res: any) => {
      if (res.status == "success") {
        this.checklist=true;
        this.experiencelist = res.content;
        this.isLoaded = true;
        this.checkingevents=false;
        $('.loading-spinner').fadeOut("slow");
      }
      else {
        $('.loading-spinner').fadeOut("slow");
        this.isLoaded = true;
        this.checkingevents=true;
      }
    });

    this.adminService.getCities().subscribe((res: any) => {
      if (res.status == "success") {
        this.citylist = res.data;
        $('.loading-spinner').fadeOut("slow");
        if(this.searchdetails!=null){
          this.searchForm.setValue(this.searchdetails);
        }
        
      }
      else {
        $('.loading-spinner').fadeOut("slow");
      }
    });

    this.searchForm = this.formBuilder.group({
      events: [''],
      city: [''],
      category: [''],
      });
  }
  get g() { return this.preForm.controls; }
  get f() { return this.registerForm.controls; }
  
  opendetailedpage(experience,schedule_type) {
    localStorage.removeItem('schedule_type');
	 localStorage.removeItem('experienceid');
    localStorage.setItem('experienceid', experience.id);
    localStorage.setItem('eventid',experience.event_id);
    this.router.navigate(['/', 'opportunitydetail']);
    localStorage.setItem('schedule_type', schedule_type);
  }
  scheduleButton(events){
    localStorage.removeItem('exploreschedule_id');
    localStorage.setItem('exploreschedule_id',events.event_id);
		$('#Modalschedule').css('display','block');
	}
	sendtologin(){
    localStorage.removeItem('exploreschedule_id');
		localStorage.setItem('exploreschedule_id','');
		this.router.navigate(['/','signin']);
	}
	modelDissmiss(){
		$('#Modalschedule').css('display','none');
	}
  Search(){
     $('.loading-spinner').show();
    this.adminService.getFilteredlist(this.searchForm.value).subscribe((res: any) => {
      if (res.status == "success") {
        this.checklist=true;
        this.experiencelist = res.content;
        this.isLoaded = true;
        this.checkingevents=false;
         $('.loading-spinner').fadeOut("slow");
      }
      else {
        this.checklist=false;
        this.checkingevents=true;
        $('.loading-spinner').fadeOut("slow");
      }
    });
  }
 
  // onClickMe(event){
  //   $('.loading-spinner').show();
  //   this.event_id = event.target.value;
  //   if(this.category ==undefined){
  //     this.category = "";
  //   }
   
   
  //   this.adminService.getFilteredlistcatogery(this.city_id,this.events,this.category).subscribe((res: any) => {
  //     if (res.status == "success") {
  //       this.checklist=true;
  //       this.experiencelist = res.content;
  //       this.isLoaded = true;
  //       this.checkingevents=false;
  //        $('.loading-spinner').fadeOut("slow");
  //     }
  //     else {
  //       this.checklist=false;
  //       this.checkingevents=true;
  //       $('.loading-spinner').fadeOut("slow");
  //     }
  //   });
  // }
 
  oncitySelect(event){
    $('.loading-spinner').show();
    this.city_id = event.target.value;
   
    if(this.events ==undefined){
      this.events = "";
    }
    if(this.category==undefined)
    {
      this.category="";
    }
    
    this.SearchCategory = {
      "city":this.city_id,
      "events":this.events,
      "category":this.category
    }
    
    this.adminService.getFilteredlist(this.SearchCategory).subscribe((res: any) => {
      if (res.status == "success") {
        this.checklist=true;
        this.experiencelist = res.content;
        this.isLoaded = true;
        this.checkingevents=false;
         $('.loading-spinner').fadeOut("slow");
      }
      else {
        this.checklist=false;
        this.checkingevents=true;
        $('.loading-spinner').fadeOut("slow");
      }
    });
  }
  
  onCategorySelect(event) {
    $('.loading-spinner').show();
    this.category = event.target.value;
     
    if(this.events ==undefined){
      this.events = "";
    }
    if(this.city==undefined)
    {
      this.city="";
    }
    
    this.SearchCategory = {
      "city":this.city,
      "events":this.events,
      "category":this.category
    }
  
    this.adminService.getFilteredlist(this.SearchCategory).subscribe((res: any) => {
      
      if (res.status == "success") {
        this.checklist=true;
        this.experiencelist = res.content;
        this.isLoaded = true;
        this.checkingevents=false;
         $('.loading-spinner').fadeOut("slow");
      }
      else {
        this.checklist=false;
        this.checkingevents=true;
        $('.loading-spinner').fadeOut("slow");
      }
    });
}


  onSubmit() {
		
		// stop here if form is invalid
		if (this.registerForm.invalid) {
			this.submitted = true;
	
			return;
		}
    this.userid = localStorage.getItem('volunteeruserid');
    this.scheduleid = localStorage.getItem('exploreschedule_id');
		this.adminService.Customrequest(this.userid,this.scheduleid,this.registerForm.value.organization,this.registerForm.value.volunteers,this.registerForm.value.description,this.registerForm.value.date,this.registerForm.value.location).subscribe((res:any)=>{
			if(res.status == "success"){
				this.flashMessage.show(res.message, { cssClass: 'alert-success', timeout: 8000 });
			this.registerForm.reset();
				this.submitted=false;
			}
			else{
				
				this.flashMessage.show(res.message, { cssClass: 'alert-danger', timeout: 8000 });
				this.submitted=false;
        this.registerForm.reset();
        
			}
		});
  }
  tagon() {
    $('.loading-spinner').show();
    this.userid = localStorage.getItem('volunteeruserid');
    if (this.preForm.invalid) {
      this.submitted = true;
      $('.loading-spinner').hide();
      return;
      }
     
    this.adminService.tagon(this.preForm.value.location, this.userid).subscribe((res: any) => {
     
      if (res.status == "success") {
        
        
       
        
        this.contactmessage = true;
        this.message = res.message;
        this.submitted = false;
        this.preForm.reset();
        $('.loading-spinner').hide();
      }
      else {
        this.contactmessage = true;
        this.message = res.message;
        this.preForm.reset();
        this.submitted = false;
        $('.loading-spinner').hide();
      }
    });
  }

  knowmore(experience){
    localStorage.removeItem('experienceid');
    localStorage.setItem('experienceid', experience.id);
    localStorage.setItem('eventid', experience.event_id);
    // alert(experience.event_id);
    this.router.navigate(['/','opportunitydetail']);
  }
  close() {
    this.contactmessage = false;
    $('#Modallocation').css('display', 'none');
  }
  locationButton(events) {
    this.userid = localStorage.getItem('volunteeruserid');
  
    this.adminService.locations(this.userid, events.id).subscribe((res: any) => {
        console.log(res);
      if (res.status == "success") {
        this.location = res.data;
       $('.loading-spinner').hide();
        this.locations = res.data;
       
      }
      else {
        $('.btn-location').css('display','none');
        $("#message").html("You have already applied for this opportunity");
        $('.loading-spinner').hide();
      }
    });
    $('#Modallocation').css('display', 'block');
  }
  modelClose() {
	$('#Modallocation').css('display', 'none');
}

onRatingSet(experience){
  localStorage.setItem('event_id',experience.event_id);
  localStorage.setItem('experience_id',experience.id);
  localStorage.setItem('user_taggedid',this.userid);
  localStorage.setItem('homepage','true');

  this.router.navigate(['/','addreview']);
}

}
